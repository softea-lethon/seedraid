## Interface: 70300
## Title: Seed Raid
## Author: Softea-Lethon
## Version: 1.0.3
## Notes: Facilitates seed raids in WoW Legion.
## SavedVariables: SeedRaidSaves
## OptionalDeps: LibDBIcon-1.0

#@no-lib-strip@
libs\LibDBIcon-1.0\LibStub\LibStub.lua
libs\LibDBIcon-1.0\CallbackHandler-1.0\CallbackHandler-1.0.lua
libs\LibDBIcon-1.0\LibDataBroker-1.1\LibDataBroker-1.1.lua
libs\LibDBIcon-1.0\LibDBIcon-1.0\LibDBIcon-1.0.lua
#@end-no-lib-strip@

ui.xml
main.lua
